package org.mokolo.meet.tests;

import static org.junit.Assert.*;


import java.io.File;
import java.util.List;

import org.junit.Test;
import org.mokolo.meet.Util;
import org.mokolo.meet.Pricing;
import org.mokolo.meet.Sample;

public class UtilTest {
  @Test
  public void canParseSamplesFile() throws Exception {
    File csvFile = new File("target/test-classes/samples/2015/01/01.samples".replaceAll("/", File.separator));
    assertTrue(csvFile.exists());
    
    List<Sample> list = Util.parseCSV(csvFile);
    
    assertEquals("Sample count", 24*12, list.size());
    
    Sample sample = list.get(0);
    assertEquals("Cumulative electricity IN 1", 1909.27, sample.getElectricityTotalKWHInMap().get(Pricing.LOW), 0.001);
    assertEquals("Current electricity IN", 0.14, sample.getElectricityCurrentKWIn(), 0.001);
    
  }
}
