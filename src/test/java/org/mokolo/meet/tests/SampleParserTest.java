package org.mokolo.meet.tests;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.Test;
import org.mokolo.meet.Pricing;
import org.mokolo.meet.Sample;
import org.mokolo.meet.SampleParser;

public class SampleParserTest {

  @Test
  public void canParseSample() throws Exception {
    SampleParser parser = new SampleParser();
    parser.kamstrup();
    
    InputStream is = this.getClass().getResourceAsStream("/sample.txt");
    Sample sample = parser.parse(is);
    is.close();
    
    Float value = null;
    
    value = sample.getElectricityTotalKWHInMap().get(Pricing.LOW);
    assertNotNull(value);
    assertEquals(1906.773, value.floatValue(), 0.001);

    value = sample.getElectricityTotalKWHInMap().get(Pricing.HIGH);
    assertNotNull(value);
    assertEquals(1296.743, value.floatValue(), 0.001);

    value = sample.getElectricityTotalKWHOutMap().get(Pricing.LOW);
    assertNotNull(value);
    assertEquals(0, value.floatValue(), 0.001);

    value = sample.getElectricityTotalKWHOutMap().get(Pricing.HIGH);
    assertNotNull(value);
    assertEquals(0, value.floatValue(), 0.001);

    value = sample.getElectricityCurrentKWIn();
    assertNotNull(value);
    assertEquals(0.21, value.floatValue(), 0.001);

    value = sample.getElectricityCurrentKWOut();
    assertNotNull(value);
    assertEquals(0, value.floatValue(), 0.001);

    value = sample.getGasTotalM3In();
    assertNotNull(value);
    assertEquals(481.222, value.floatValue(), 0.001);
  }
}
