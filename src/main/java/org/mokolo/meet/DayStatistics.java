package org.mokolo.meet;

import java.util.ArrayList;


import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.SubCategoryAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.data.KeyToGroupMap;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import lombok.Getter;

@XmlRootElement(name = "day-statistics")
public class DayStatistics {
  
  private DateTime day;
  @Getter private List<SampleStatistics> samplesStatistics;
  @Getter private List<HourStatistics> hoursStatistics;
  @Getter private ResourceDiffs resourceDiffs;

  private DayStatistics() {
    this.samplesStatistics = new ArrayList<>();
    this.hoursStatistics = new ArrayList<>();
    this.resourceDiffs = new ResourceDiffs();
  }
  
  public DayStatistics(int year, int monthOfYear, int dayOfMonth) {
    this();
    this.day = new DateTime(year, monthOfYear, dayOfMonth, 0, 0);
  }

  public long getDay() {
    return this.day.getMillis();
  }
  
  @XmlAttribute(name = "first-millisecond")
  public void setDay(long ms) {
    Date date = new Date(ms);
    this.day = new DateTime(date);
  }

  public int getYear() {
    return this.day.getYear();
  }
  
  @XmlAttribute
  public void setYear(int year) {
    // Don't do anything; only used to inform human in generated XML
  }
  
  public int getMonth() {
    return this.day.getMonthOfYear();
  }
  
  @XmlAttribute
  public void setMonth(int month) {
    // Don't do anything; only used to inform human in generated XML
  }
  
  public int getDayOfMonth() {
    return this.day.getDayOfMonth();
  }
  
  @XmlAttribute(name = "day")
  public void setDayOfMonth(int dayOfMonth) {
    // Don't do anything; only used to inform human in generated XML
  }
  
  @XmlElementWrapper(name = "samples")
  @XmlElement(name="sample-statistics")
  public void setSamplesStatistics(List<SampleStatistics> list) {
    this.samplesStatistics = list;
  }
  
  @XmlElementWrapper(name = "hours")
  @XmlElement(name="hour-statistics")
  public void setHoursStatistics(List<HourStatistics> list) {
    this.hoursStatistics = list;
  }
  
  @XmlElement(name = "diffs")
  public void setResourceDiffs(ResourceDiffs diffs) {
    this.resourceDiffs = diffs;
  }

  private CategoryDataset createDatasetElectricityHours() {
    DefaultCategoryDataset result = new DefaultCategoryDataset();
    for (HourStatistics stats : this.hoursStatistics) {
      String hour = String.format("%02d", stats.getHourOfDay());
      result.addValue(stats.getResourceDiffs().getElectricityKWHInLow().getEstimate(), "Low in", hour);
      result.addValue(stats.getResourceDiffs().getElectricityKWHInHigh().getEstimate(), "High in", hour);
    }
    return result;
  }

  public JFreeChart createChartElectricityHours() {
    DateTimeFormatter fmt = DateTimeFormat.forPattern("E"); // use 'E' for short abbreviation (Mon, Tues, etc)
    String dayOfWeekStr = fmt.withLocale(Locale.getDefault()).print(this.day);
    String title = String.format("Electricity %s %4d-%02d-%02d", dayOfWeekStr, this.day.getYear(), this.day.getMonthOfYear(), this.day.getDayOfMonth());
    final JFreeChart chart = ChartFactory.createStackedBarChart(
        title,
        "Category",                  // domain axis label
        "KWH",                       // range axis label
        this.createDatasetElectricityHours(),  // data
        PlotOrientation.VERTICAL,    // the plot orientation
        true,                        // legend
        true,                        // tooltips
        false                        // urls
    );
    
    GroupedStackedBarRenderer renderer = new GroupedStackedBarRenderer();
    KeyToGroupMap map = new KeyToGroupMap("G1");
    map.mapKeyToGroup("Low in", "G1");
    map.mapKeyToGroup("High in", "G1");
    renderer.setSeriesToGroupMap(map); 
        
    SubCategoryAxis domainAxis = new SubCategoryAxis("Electricity / hour");
    domainAxis.setCategoryMargin(0.05);
    
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    plot.setDomainAxis(domainAxis);
    plot.setRenderer(renderer);
    return chart;    
  }

  private CategoryDataset createDatasetGasHours() {
    DefaultCategoryDataset result = new DefaultCategoryDataset();
    for (HourStatistics stats : this.hoursStatistics) {
      String hour = String.format("%02d", stats.getHourOfDay());
      result.addValue(stats.getResourceDiffs().getGasM3In().getEstimate(), "Gas in", hour);
    }
    return result;
  }

  public JFreeChart createChartGasHours() {
    DateTimeFormatter fmt = DateTimeFormat.forPattern("E"); // use 'E' for short abbreviation (Mon, Tues, etc)
    String dayOfWeekStr = fmt.withLocale(Locale.getDefault()).print(this.day);
    String title = String.format("Gas %s %4d-%02d-%02d", dayOfWeekStr, this.day.getYear(), this.day.getMonthOfYear(), this.day.getDayOfMonth());
    final JFreeChart chart = ChartFactory.createStackedBarChart(
        title,
        "Category",                  // domain axis label
        "M3",                        // range axis label
        this.createDatasetGasHours(),// data
        PlotOrientation.VERTICAL,    // the plot orientation
        true,                        // legend
        true,                        // tooltips
        false                        // urls
    );
    
    GroupedStackedBarRenderer renderer = new GroupedStackedBarRenderer();
    KeyToGroupMap map = new KeyToGroupMap("G1");
    map.mapKeyToGroup("Gas in", "G1");
    renderer.setSeriesToGroupMap(map); 
        
    SubCategoryAxis domainAxis = new SubCategoryAxis("Gas / hour");
    domainAxis.setCategoryMargin(0.05);
    
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    plot.setDomainAxis(domainAxis);
    plot.setRenderer(renderer);
    return chart;    
  }
  
  private XYDataset createDatasetElectricityCurrent() {
    final TimeSeriesCollection dataset = new TimeSeriesCollection();
    final TimeSeries in = new TimeSeries("In");
    final TimeSeries out = new TimeSeries("Out");
    for (SampleStatistics stats : this.samplesStatistics) {
      Date timestamp = new Date(stats.getTimestamp().longValue());
      Millisecond ms = new Millisecond(timestamp);
      if (stats.getElectricityKWInCurrent() != null)
        in.add(ms, stats.getElectricityKWInCurrent());
      if (stats.getElectricityKWOutCurrent() != null)
        out.add(ms, -stats.getElectricityKWOutCurrent());
    }
    dataset.addSeries(in);
    dataset.addSeries(out);
    return dataset;
  }

  public JFreeChart createChartElectricityCurrent() {
    DateTimeFormatter fmt = DateTimeFormat.forPattern("E"); // use 'E' for short abbreviation (Mon, Tues, etc)
    String dayOfWeekStr = fmt.withLocale(Locale.getDefault()).print(this.day);
    String title = String.format("Electricity %s %4d-%02d-%02d", dayOfWeekStr, this.day.getYear(), this.day.getMonthOfYear(), this.day.getDayOfMonth());

    final JFreeChart chart = ChartFactory.createTimeSeriesChart(
        title,
        "Time", "KW",
        this.createDatasetElectricityCurrent(),
        true,
        true,
        false
    );

    final XYPlot plot = chart.getXYPlot();
    plot.setDomainCrosshairVisible(true);
    plot.setRangeCrosshairVisible(true);
    plot.setRenderer(new XYAreaRenderer());
    
    return chart;
  }
  
}
