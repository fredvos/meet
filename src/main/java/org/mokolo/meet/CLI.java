package org.mokolo.meet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.mokolo.commons.command.CommandDefinition;
import org.mokolo.commons.command.CommandParser;
import org.mokolo.commons.lang.Log4JConfiguration;
import org.mokolo.commons.lang.document.Document;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import lombok.Cleanup;
import lombok.extern.log4j.Log4j;

@Log4j
public class CLI {

  public static void main(String[] args) {

    try {
      CommandParser commandParser = new CommandParser(new OptionParser("v"), "request");
      commandParser.addCommandDefinition(new CommandDefinition("process-samples", "ps")
          .addRequiredArgument("year")
          .addRequiredArgument("month")
          .addRequiredArgument("day"));
      OptionSet options = commandParser.getOptionParser().parse(args);

      Log4JConfiguration log4j = new Log4JConfiguration();
      Log4JConfiguration.Level level = Log4JConfiguration.Level.VERBOSE;
      if (options.has("v"))
        level = Log4JConfiguration.Level.VERBOSE;
      log4j.addConsoleAppender(level, Log4JConfiguration.NONE);
      log4j.addLogger("org.mokolo.commons", Log4JConfiguration.Level.NORMAL);
      log4j.addLogger("org.mokolo.meet", level);

      Document document = commandParser.parseCommandWithSwitches(options);
      commandParser.validateDocument(document);
      
      if (document.getValue("request").equals("process-samples")) {
        int inputYear = document.getValueAsInt("year");
        int inputMonth = document.getValueAsInt("month");
        int inputDay = document.getValueAsInt("day");

        DayStatistics stats = Util.computeDayStatistics(inputYear, inputMonth, inputDay);
        
        /*
         * Generate XML:
         */
        File targetYearDirectory = new File(Configuration.getConfiguration().getTargetDirectory(), String.valueOf(inputYear));
        if (! targetYearDirectory.exists())
          targetYearDirectory.mkdirs();
        File targetMonthDirectory = new File(targetYearDirectory, String.format("%02d", inputMonth));
        if (! targetMonthDirectory.exists())
          targetMonthDirectory.mkdir();
        File targetXml = new File(targetMonthDirectory, String.format("%02d.xml", inputDay));

        JAXBContext jaxbContext = JAXBContext.newInstance(DayStatistics.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        @Cleanup OutputStream os = new FileOutputStream(targetXml);
        marshaller.marshal(stats, os);
        
        JFreeChart chart;
        
        /*
         * Generate PNG electricity / hour:
         */
        chart = stats.createChartElectricityHours();
        File targetElectricityHours = new File(targetMonthDirectory, String.format("%02d.elhours.png", inputDay));
        ChartUtilities.saveChartAsPNG(targetElectricityHours, chart, 600, 400);

        /*
         * Generate PNG electricity plot:
         */
        chart = stats.createChartElectricityCurrent();
        File targetElectricityCurrent = new File(targetMonthDirectory, String.format("%02d.elcurrent.png", inputDay));
        ChartUtilities.saveChartAsPNG(targetElectricityCurrent, chart, 600, 400);

        /*
         * Generate PNG gas / hour:
         */
        chart = stats.createChartGasHours();
        File targetGasHours = new File(targetMonthDirectory, String.format("%02d.gashours.png", inputDay));
        ChartUtilities.saveChartAsPNG(targetGasHours, chart, 600, 400);
      }

    } catch (Exception e) {
      log.error(e.toString());
      e.printStackTrace();
    }
  }

}
