package org.mokolo.meet;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.jfree.data.time.Hour;

import lombok.Getter;

public class HourStatistics {
  @Getter private Long firstMillisecond;
  private Hour hour;
  private ResourceDiffs resourceDiffs;
  
  public HourStatistics() {
    this.hour = new Hour();
    this.firstMillisecond = this.hour.getFirstMillisecond();
    this.resourceDiffs = new ResourceDiffs();
  }
  
  public HourStatistics(Long firstMillisecond) {
    this();
    this.setFirstMillisecond(firstMillisecond);
  }
  
  @XmlAttribute(name = "first-millisecond")
  public void setFirstMillisecond(Long firstMillisecond) {
    this.firstMillisecond = firstMillisecond;
    this.hour = new Hour(new Date(this.firstMillisecond));
  }
  
  public int getHourOfDay() {
    return this.hour.getHour();
  }
  
  @XmlAttribute(name = "hour")
  public void setHourOfDay(int hourOfDay) {
    // Don't do anything; only used to inform human in generated XML
  }
  
  public ResourceDiffs getResourceDiffs() {
    return this.resourceDiffs;
  }

  @XmlElement(name = "diffs")
  public void setResourceDiffs(ResourceDiffs diffs) {
    this.resourceDiffs = diffs;
  }

}
