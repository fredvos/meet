package org.mokolo.meet;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import lombok.Getter;
import lombok.Setter;

/**
 * Single sample from meter
 *
 */
public class Sample {
  @Getter @Setter private Long timestamp;
  @Getter private Map<Pricing, Float> electricityTotalKWHInMap;
  @Getter private Map<Pricing, Float> electricityTotalKWHOutMap;
  @Getter Float electricityCurrentKWIn;
  @Getter Float electricityCurrentKWOut;
  @Getter Float gasTotalM3In;
  
  private static SimpleDateFormat isoDateHourGMTFormat = new SimpleDateFormat("yyyy-MM-dd:HHZ");
  
  static {
    isoDateHourGMTFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
  }
  
  public Sample() {
    this.electricityTotalKWHInMap = new HashMap<>();
    this.electricityTotalKWHOutMap = new HashMap<>();
  }
  
  public void put(String key, Float value) {
    if      (key.equals("EIT1") || key.equals("ElAfnCum1")) this.electricityTotalKWHInMap.put(Pricing.LOW, value);
    else if (key.equals("EIT2") || key.equals("ElAfnCum2")) this.electricityTotalKWHInMap.put(Pricing.HIGH, value);
    else if (key.equals("EOT1") || key.equals("ElLevCum1")) this.electricityTotalKWHOutMap.put(Pricing.LOW, value);
    else if (key.equals("EOT2") || key.equals("ElLevCum2")) this.electricityTotalKWHOutMap.put(Pricing.HIGH, value);
    else if (key.equals("EIC") || key.equals("ElAfnCur"))   this.electricityCurrentKWIn = value;
    else if (key.equals("EOC") || key.equals("ElLevCur"))   this.electricityCurrentKWOut = value;
    else if (key.equals("GIT") || key.equals("GasAfnCum"))  this.gasTotalM3In = value;
  }
  
  public Float getElectricityTotalKWHIn() {
    return sum(this.electricityTotalKWHInMap.values());
  }
  
  public Float getElectricityTotalKWHOut() {
    return sum(this.electricityTotalKWHOutMap.values());
  }
  
  public static Float sum(Collection<Float> values) {
    if (values == null || values.isEmpty())
      return null;
    else {
      Float f = new Float(0.0);
      for (Float entry : values)
        f += entry;
      return f;
    }
  }
  
  public SampleStatistics getSampleStatistics() {
    return new SampleStatistics(this.timestamp, this.electricityCurrentKWIn, this.electricityCurrentKWOut);
  }
}
