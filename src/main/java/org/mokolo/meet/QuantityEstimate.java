package org.mokolo.meet;

import javax.xml.bind.annotation.XmlAttribute;

import lombok.Getter;

/**
 * Estimate of difference in values between start and end of time period
 * 
 * Used for time series.
 * 
 */
public class QuantityEstimate {
  @Getter private Float estimate;
  @Getter private Float minimum;
  @Getter private Float maximum;
  
  public QuantityEstimate() { }
  
  public QuantityEstimate(Float estimate, Float minimum, Float maximum) {
    this();
    this.estimate = estimate;
    this.minimum = minimum;
    this.maximum = maximum;
  }
  
  @XmlAttribute
  public void setEstimate(Float f) {
    this.estimate = f;
  }

  @XmlAttribute
  public void setMinimum(Float f) {
    this.minimum = f;
  }

  @XmlAttribute
  public void setMaximum(Float f) {
    this.maximum = f;
  }
}
