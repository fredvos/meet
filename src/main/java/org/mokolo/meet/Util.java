package org.mokolo.meet;

import java.io.File;


import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;

public class Util {

  public static DayStatistics computeDayStatistics(int year, int monthOfYear, int dayOfMonth)
  throws IOException {
    Map<Pricing, InterpolatingMap> electricityKWHInMap = new HashMap<>();
    electricityKWHInMap.put(Pricing.LOW, new InterpolatingMap());
    electricityKWHInMap.put(Pricing.HIGH, new InterpolatingMap());
    Map<Pricing, InterpolatingMap> electricityKWHOutMap = new HashMap<>();
    electricityKWHOutMap.put(Pricing.LOW, new InterpolatingMap());
    electricityKWHOutMap.put(Pricing.HIGH, new InterpolatingMap());
    InterpolatingMap gasM3In = new InterpolatingMap();

    DayStatistics dayStatistics = new DayStatistics(year, monthOfYear, dayOfMonth);

    /*
     * Load samples of day before this day, this day and the day after this day.
     * Check if important values are not null.
     * If so, add the value to the inperpolating map of values.
     */
    for (int offsetDay= -1; offsetDay <= 1; offsetDay++) {
      List<Sample> samples = loadSamples(year, monthOfYear, dayOfMonth, offsetDay);
      for (Sample sample : samples) {
        if (sample.getElectricityTotalKWHInMap().get(Pricing.LOW) != null)
          electricityKWHInMap.get(Pricing.LOW).put(sample.getTimestamp(), sample.getElectricityTotalKWHInMap().get(Pricing.LOW));
        if (sample.getElectricityTotalKWHInMap().get(Pricing.HIGH) != null)
          electricityKWHInMap.get(Pricing.HIGH).put(sample.getTimestamp(), sample.getElectricityTotalKWHInMap().get(Pricing.HIGH));
        if (sample.getElectricityTotalKWHOutMap().get(Pricing.LOW) != null)
          electricityKWHOutMap.get(Pricing.LOW).put(sample.getTimestamp(), sample.getElectricityTotalKWHOutMap().get(Pricing.LOW));
        if (sample.getElectricityTotalKWHOutMap().get(Pricing.HIGH) != null)
          electricityKWHOutMap.get(Pricing.HIGH).put(sample.getTimestamp(), sample.getElectricityTotalKWHOutMap().get(Pricing.HIGH));
        if (sample.getGasTotalM3In() != null)
          gasM3In.put(sample.getTimestamp(), sample.getGasTotalM3In());
        
        if (offsetDay == 0)
          dayStatistics.getSamplesStatistics().add(sample.getSampleStatistics());
      }
    }
    
    /*
     * For each hour of the day estimate the five quantities of electricity
     * and gas used or produced during this hour.
     * Generate an HourStatistics object per hour and store.
     */
    DateTime day = new DateTime(year, monthOfYear, dayOfMonth, 0, 0);
    DateTime forHour = new DateTime(year, monthOfYear, dayOfMonth, 0, 0);
    while (forHour.getDayOfMonth() == dayOfMonth) {
      DateTime nextHour = forHour.plusHours(1);
      long start = forHour.getMillis();
      long end = nextHour.getMillis();
      HourStatistics hStats = new HourStatistics(forHour.getMillis());
      ResourceDiffs diffs = computeDiffs(electricityKWHInMap, electricityKWHOutMap, gasM3In, start, end);
      hStats.setResourceDiffs(diffs);
      dayStatistics.getHoursStatistics().add(hStats);
      forHour = nextHour;
    }
    
    DateTime nextDay = day.plusDays(1);
    long start = day.getMillis();
    long end = nextDay.getMillis();
    ResourceDiffs diffs = computeDiffs(electricityKWHInMap, electricityKWHOutMap, gasM3In, start, end);
    dayStatistics.setResourceDiffs(diffs);
    

    return dayStatistics;
  }

  private static ResourceDiffs computeDiffs(Map<Pricing, InterpolatingMap> electricityKWHInMap,
      Map<Pricing, InterpolatingMap> electricityKWHOutMap, InterpolatingMap gasM3In, long start, long end) {
    ResourceDiffs diffs = new ResourceDiffs();
    diffs.setElectricityKWHInLow(electricityKWHInMap.get(Pricing.LOW).estimateQuantity(start, end));
    diffs.setElectricityKWHInHigh(electricityKWHInMap.get(Pricing.HIGH).estimateQuantity(start, end));
    diffs.setElectricityKWHOutLow(electricityKWHOutMap.get(Pricing.LOW).estimateQuantity(start, end));
    diffs.setElectricityKWHOutHigh(electricityKWHOutMap.get(Pricing.HIGH).estimateQuantity(start, end));
    diffs.setGasM3In(gasM3In.estimateQuantity(start, end));
    return diffs;
  }
  
  public static List<Sample> loadSamples(int year, int monthOfYear, int dayOfMonth, int offsetDays)
  throws IOException {
    DateTime baseDate = new DateTime(year, monthOfYear, dayOfMonth, 0, 0);
    DateTime date;
    if (offsetDays < 0)
      date = baseDate.minusDays(0 - offsetDays);
    else if (offsetDays > 0)
      date = baseDate.plusDays(offsetDays);
    else
      date = baseDate;
    File samplesYearDirectory = new File(Configuration.getConfiguration().getSamplesDirectory(), String.valueOf(date.getYear()));
    File samplesMonthDirectory = new File(samplesYearDirectory, String.format("%02d", date.getMonthOfYear()));
    File samplesFile = new File(samplesMonthDirectory, String.format("%02d.samples", date.getDayOfMonth()));
    return parseCSV(samplesFile);
  }
  
  public static List<Sample> parseCSV(File csvFile) throws IOException {
    List<Sample> list = new ArrayList<>(); 
    
    CSVParser parser = CSVParser.parse(csvFile, Charset.defaultCharset(), SampleWriter.csvFormat.withHeader("timestamp","year","month","day","hour","minute","second","EIT1","EIT2","EOT1","EOT2","EIC","EOC","GIT"));
    boolean headerSkipped = false;
    for (CSVRecord record : parser.getRecords()) {
      if (headerSkipped == false)
        headerSkipped = true;
      else
        list.add(toSample(record));
    }
    
    return list;
  }
  
  private static Sample toSample(CSVRecord record) {
    //log.debug("Record: "+record.toString());
    
    Sample sample = new Sample();
    
    //log.debug("The entries:");
    Map<String, String> map = record.toMap();
    //for (Map.Entry<String, String> entry : map.entrySet()) {
    //  log.debug(entry.getKey()+" = "+entry.getValue());
    //}
    
    String value;
    
    value = map.get("timestamp");
    sample.setTimestamp(Long.parseLong(value));

    for (Map.Entry<String, String> entry : map.entrySet()) {
      value = map.get(entry.getKey());
      if (value != null && value.length() > 0) {
        float floatvalue = Float.parseFloat(value);
        sample.put(entry.getKey(), new Float(floatvalue));
      }
    }
    
    return sample;
  }
}
