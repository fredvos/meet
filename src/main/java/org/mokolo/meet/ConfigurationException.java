package org.mokolo.meet;

public class ConfigurationException extends Exception {
  private static final long serialVersionUID = -7207713905316875828L;

  public ConfigurationException(String msg) {
    super(msg);
  }
}
