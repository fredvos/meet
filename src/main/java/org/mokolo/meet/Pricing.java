package org.mokolo.meet;

public enum Pricing {
  LOW, HIGH;

  /* https://www.google.nl/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&ved=0ahUKEwj1vZ3Xhv3XAhXI1qQKHQ-YDGMQFgg4MAE&url=https%3A%2F%2Fwww.enexis.nl%2FDocuments%2Fhandleidingen%2FKamstrup_ZABF_en_KA6U_code_2016-8087.pdf&usg=AOvVaw1Hqf2ov-_OSfwqtGMVv4qo */
  public static Pricing translate(Integer number) {
    return Pricing.values()[number-1];
  }
}
