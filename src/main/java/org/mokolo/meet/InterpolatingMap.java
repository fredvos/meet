package org.mokolo.meet;

import java.util.Map;
import java.util.TreeMap;

public class InterpolatingMap extends TreeMap<Long, Float> {
  private static final long serialVersionUID = 2420567331554906945L;

  /**
   * Returns value from map if key present or else interpolates the value
   * 
   * Returns null if no key less than or equal to given key present
   * or no key greater than or equal to given key present.
   */
  public ValueEstimate estimateValue(Long key) {
    Float value = this.get(key);
    if (value != null)
      return new ValueEstimate(value, value, value);
    else {
      Map.Entry<Long, Float> floor = this.floorEntry(key);
      Map.Entry<Long, Float> ceiling = this.ceilingEntry(key);
      if (floor == null || ceiling == null)
        return null;
      else {
        long floorTimeToCeilingTime = ceiling.getKey() - floor.getKey();
        long floorTimeToKey = key - floor.getKey();
        float floorValueToCeilingValue = ceiling.getValue().floatValue() - floor.getValue().floatValue();
        float frac = 1.0F * floorTimeToKey / floorTimeToCeilingTime;
        value = new Float(floor.getValue().floatValue() + frac * floorValueToCeilingValue);
//        if (key == 1420200000000L) {
//          System.out.println("Value, floor, ceiling = "+value+", "+floor.getValue()+", "+ceiling.getValue());
//        }
        return new ValueEstimate(value, floor.getValue(), ceiling.getValue());
      }
    }
  }
  
  public QuantityEstimate estimateQuantity(Long start, Long end) {
    ValueEstimate ipStart = this.estimateValue(start);
    ValueEstimate ipEnd = this.estimateValue(end);
    if (ipStart == null || ipEnd == null)
      return null;
    else {
      float estimateOfDifference = ipEnd.getEstimate().floatValue() - ipStart.getEstimate().floatValue();
      float minimumDifference = ipEnd.getMinimum().floatValue() - ipStart.getMaximum();
      float maximumDifference = ipEnd.getMaximum().floatValue() - ipStart.getMinimum();
      return new QuantityEstimate(estimateOfDifference, minimumDifference, maximumDifference);
    }
  }
}
