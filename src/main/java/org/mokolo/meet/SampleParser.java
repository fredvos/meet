package org.mokolo.meet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SampleParser {
  private Map<String, Pattern> patterns;
  
  public SampleParser() {
    this.patterns = new HashMap<String, Pattern>();
  }
  
  public void setPattern(String key, String patternString) {
    this.patterns.put(key,  Pattern.compile(patternString));
  }
  
  public Sample parse(InputStream is) throws IOException {
    Sample sample = new Sample();
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    String line;
    while ((line = reader.readLine()) != null) {
      for (Map.Entry<String, Pattern> entry : this.patterns.entrySet()) {
        Matcher matcher = entry.getValue().matcher(line);
        if (matcher.find()) {
          String firstGroup = matcher.group(1);
          float floatValue = Float.parseFloat(firstGroup);
          sample.put(entry.getKey(), new Float(floatValue));
        }
      }
    }
    return sample;
  }

  public void kamstrup() {
    this.setPattern("EIT1", "1\\.8\\.1\\((\\d+\\.\\d+)\\*kWh\\)");
    this.setPattern("EIT2", "1\\.8\\.2\\((\\d+\\.\\d+)\\*kWh\\)");
    this.setPattern("EOT1", "2\\.8\\.1\\((\\d+\\.\\d+)\\*kWh\\)");
    this.setPattern("EOT2", "2\\.8\\.2\\((\\d+\\.\\d+)\\*kWh\\)");
    this.setPattern("EIC", "1\\.7\\.0\\((\\d+\\.\\d+)\\*kW\\)");
    this.setPattern("EOC", "2\\.7\\.0\\((\\d+\\.\\d+)\\*kW\\)");
    this.setPattern("GIT", "^\\((\\d+\\.\\d+)\\)$");
  }

}
