package org.mokolo.meet;

import javax.xml.bind.annotation.XmlAttribute;

import lombok.Getter;

public class ValueEstimate {
  @Getter private Float estimate;
  @Getter private Float minimum;
  @Getter private Float maximum;
  
  public ValueEstimate() { }
  
  public ValueEstimate(Float estimate, Float minimum, Float maximum) {
    this();
    this.estimate = estimate;
    this.minimum = minimum;
    this.maximum = maximum;
  }
  
  @XmlAttribute
  public void setEstimate(Float f) {
    this.estimate = f;
  }

  @XmlAttribute
  public void setMinimum(Float f) {
    this.minimum = f;
  }

  @XmlAttribute
  public void setMaximum(Float f) {
    this.maximum = f;
  }
}
