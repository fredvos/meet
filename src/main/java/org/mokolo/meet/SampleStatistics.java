package org.mokolo.meet;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;

import org.joda.time.DateTime;

import lombok.Getter;

public class SampleStatistics {

  private DateTime timestamp;
  @Getter private Float electricityKWInCurrent;
  @Getter private Float electricityKWOutCurrent;

  public SampleStatistics(long milliseconds, Float electricityKWInCurrent, Float electricityKWOutCurrent) {
    Date ts = new Date(milliseconds);
    this.timestamp = new DateTime(ts);
    this.electricityKWInCurrent = electricityKWInCurrent;
    this.electricityKWOutCurrent = electricityKWOutCurrent;
  }

  public Long getTimestamp() {
    return this.timestamp.getMillis();
  }
  
  @XmlAttribute(name = "millisecond")
  public void setTimestamp(Long ms) {
    Date ts = new Date(ms);
    this.timestamp = new DateTime(ts);
  }
  
  @XmlAttribute
  public void setElectricityKWInCurrent(Float f) {
    this.electricityKWInCurrent = f;
  }

  @XmlAttribute
  public void setElectricityKWOutCurrent(Float f) {
    this.electricityKWOutCurrent = f;
  }
}
