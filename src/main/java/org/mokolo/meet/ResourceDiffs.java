package org.mokolo.meet;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

import lombok.Getter;
import lombok.Setter;

public class ResourceDiffs {
  private Map<Pricing, QuantityEstimate> electricityKWHInMap;
  private Map<Pricing, QuantityEstimate> electricityKWHOutMap;
  @Getter @Setter private QuantityEstimate gasM3In;
  
  public ResourceDiffs() {
    this.electricityKWHInMap = new HashMap<>();
    this.electricityKWHOutMap = new HashMap<>();
  }
  
  public QuantityEstimate getElectricityKWHInLow() {
    return electricityKWHInMap.get(Pricing.LOW);
  }

  @XmlElement
  public void setElectricityKWHInLow(QuantityEstimate ve) {
    this.electricityKWHInMap.put(Pricing.LOW, ve);
  }
  
  public QuantityEstimate getElectricityKWHInHigh() {
    return electricityKWHInMap.get(Pricing.HIGH);
  }

  @XmlElement
  public void setElectricityKWHInHigh(QuantityEstimate ve) {
    this.electricityKWHInMap.put(Pricing.HIGH, ve);
  }
  
  public QuantityEstimate getElectricityKWHOutLow() {
    return electricityKWHOutMap.get(Pricing.LOW);
  }

  @XmlElement
  public void setElectricityKWHOutLow(QuantityEstimate ve) {
    this.electricityKWHOutMap.put(Pricing.LOW, ve);
  }

  public QuantityEstimate getElectricityKWHOutHigh() {
    return electricityKWHOutMap.get(Pricing.HIGH);
  }

  @XmlElement
  public void setElectricityKWHOutHigh(QuantityEstimate ve) {
    this.electricityKWHOutMap.put(Pricing.HIGH, ve);
  }
  
}
