package org.mokolo.meet;

import java.io.File;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.mokolo.commons.io.ConfigurationLoader;

import lombok.Getter;

public class Configuration {
  private static Configuration configuration;
  private static Object classLock = Configuration.class;

  private Properties properties;
  @Getter private File samplesDirectory;
  @Getter private File targetDirectory;
  
  private static final Logger logger = Logger.getLogger(Configuration.class);
  
  private Configuration() {
    try {
     this.properties = ConfigurationLoader.getConfiguration("meet");
     this.setFields();
     
    } catch (Exception e) {
      logger.error(e.getClass().getName() + " occurred. Msg=" + e.getMessage());
      e.printStackTrace();
    }
  }

  private void setFields () throws ConfigurationException {
    String filename;
    
    filename = this.properties.getProperty("meet.samples.directory");
    if (filename != null) {
      this.samplesDirectory = new File(filename);
    }

    filename = this.properties.getProperty("meet.target.directory");
    if (filename != null) {
      this.targetDirectory = new File(filename);
    }
  }

  public static Configuration getConfiguration() {
    synchronized (classLock) {
      if (configuration == null)
        configuration = new Configuration();
      return configuration;
    }
  }
  
}
