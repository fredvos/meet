package org.mokolo.meet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.joda.time.DateTime;
import org.mokolo.meet.Configuration;

public class SampleWriter {
  
  public static final Object [] FILE_HEADER = {"timestamp","year","month","day","hour","minute","second","EIT1","EIT2","EOT1","EOT2","EIC","EOC","GIT"};
  public static final CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
  
  public static void main(String[] args) throws IOException {
    Configuration configuration = Configuration.getConfiguration();
    DateTime now = new DateTime(new Date());
    String filePath = String.format("%4d/%02d/%02d.samples", now.getYear(), now.getMonthOfYear(), now.getDayOfMonth());
    File samplesDirectory = configuration.getSamplesDirectory();
    File dayFile = new File(samplesDirectory, filePath);
    File parentDirectory = dayFile.getParentFile();
    if (! parentDirectory.exists())
      parentDirectory.mkdirs();
    boolean printHeader = false;
    if (! dayFile.exists())
      printHeader = true;
    FileWriter fileWriter = new FileWriter(dayFile, true);
    CSVPrinter csvPrinter = new CSVPrinter(fileWriter, csvFormat);

    if (printHeader == true)
      csvPrinter.printRecord(FILE_HEADER);

    SampleParser parser = new SampleParser();
    parser.kamstrup();
    
    Sample sample = parser.parse(System.in);
    
    List<Object> record = new ArrayList<Object>();
    record.add(new Long(now.getMillis()));
    record.add(new Integer(now.getYear()));
    record.add(new Integer(now.getMonthOfYear()));
    record.add(new Integer(now.getDayOfMonth()));
    record.add(new Integer(now.getHourOfDay()));
    record.add(new Integer(now.getMinuteOfHour()));
    record.add(new Integer(now.getSecondOfMinute()));
    record.add(sample.getElectricityTotalKWHInMap().get(Pricing.LOW));
    record.add(sample.getElectricityTotalKWHInMap().get(Pricing.HIGH));
    record.add(sample.getElectricityTotalKWHOutMap().get(Pricing.LOW));
    record.add(sample.getElectricityTotalKWHOutMap().get(Pricing.HIGH));
    record.add(sample.getElectricityCurrentKWIn());
    record.add(sample.getElectricityCurrentKWOut());
    record.add(sample.getGasTotalM3In());

    csvPrinter.printRecord(record);
    csvPrinter.close();
  }
}
