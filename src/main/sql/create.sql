/*
create table SM_SAMPLES (
  SAMPLETIME timestamp with time zone not null,
  EIT1 numeric(9, 3),
  EIT2 numeric(9, 3),
  EOT1 numeric(9, 3),
  EOT2 numeric(9, 3),
  EIC numeric(9, 3),
  EOC numeric(9, 3),
  GIT numeric(9, 3)
);
alter table SM_SAMPLES add primary key (SAMPLETIME);
*/

create table SM_DAYS (
  SAMPLETIME timestamp with time zone not null,
  DOW integer not null,
  EID numeric(9, 3),
  EIE numeric(9, 3),
  EOD numeric(9, 3),
  EOE numeric(9, 3),
  GID numeric(9, 3),
  GIE numeric(9, 3)
);
alter table SM_DAYS add primary key (SAMPLETIME);

create table SM_HOURS (
  SAMPLETIME timestamp with time zone not null,
  DOW integer not null,
  EID numeric(9, 3),
  EIL numeric(9, 3),
  EIH numeric(9, 3),
  EOD numeric(9, 3),
  EOL numeric(9, 3),
  EOH numeric(9, 3),
  GID numeric(9, 3)
);
alter table SM_HOURS add primary key (SAMPLETIME);

