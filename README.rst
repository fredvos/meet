====
MEET
====

Aanleiding
++++++++++
Sinds eind 2013 heb ik een nieuwe, slimme, energiemeter, een Kamstrup 162JxC.
Vanaf dat ik die had, legde ik de eerste maanden handmatig aan het einde van de maand steeds de standen voor electriciteit, gas en water vast.
Zo kreeg ik inzicht in mijn gebruik.
De engergiemeter verzamelt alleen de data over electriciteit en gas. 
Water moest ik van een aparte meter aflezen.

Ik weet dat ik met mijn gebruik van gas heel laag zit ten opzichte van vergelijkbare woningen/huishoudens.
Dat komt door m'n houtkachel die ik dagelijks stook.
Dat is een Tigchelkachel, een accumulerende houtkachel.
Door die kachel heb ik alleen gas nodig voor het handhaven van de minimumtemperatuur in de woonkamer en voor comfort wanneer het kil is, en verder voor koken en warm water.

Met electriciteitsgebruik zit ik te hoog.
Daar wil ik wat aan doen.
Maandelijkse metingen helpen niet om inzicht te krijgen.
Daarvoor zijn regelmatiger metingen nodig.
Dat moet automatisch.

Techniek
++++++++
Elke slimme energiemeter heeft een seriële poort die een standaard-protocol gebruikt.
Met een computer kan je die uitlezen.
Ik had nog een oude Raspberry PI (model A) en een vrije connector aan m'n router in de meterkast.
Ik heb die Raspberry ingezet als middel om metingen te doen.
Ik had daarvoor wel een kabel nodig die een RJ-11 aansluiting omzet in USB.
Stom dat die meters niet zelf USB hebben, of een netwerkaansluiting.
Deze vond ik bij https://sites.google.com/site/nta8130p1smartmeter/webshop

Op de Raspberry installeerde ik Raspbian OS.
Verder installeerde ik het Python-programmaatje dat op de volgende pagina staat: http://gejanssen.com/howto/Slimme-meter-uitlezen/

Ik wilde niet allemaal software op die PI installeren, maar liet het daarbij.
Via SSH kan ik dat programmaatje aanroepen vanaf een computer die altijd aan staat, en kan ik de uitvoer parsen.

Software
++++++++
De computer die altijd aan staat is een PC met Kubuntu 16.04.
Een cron-job roept elke vijf minuten remote het Python-script aan en stuurt de output naar een Java-programma.
Dat Java-programma haalt via reguliere expressies alle gegevens uit de uitvoer van het Python-programma en slaat het op in CVS-bestanden.
Er is 1 bestand per dag.
Het Python-programmaatje of de meter is wat brak.
Het programmaatje soms halve berichten.
Het stopt met lezen na n regels, terwijl er een duidelijk einde is aan een bericht van de meter.
Ik moet dat zelf nog eens bekijken.
Door de gemankeerde berichten mis ik soms wat data.

Voorbeeld van een bericht vind je in src/test/resources.

De CSV-bestanden staan in een directory.
Deze heeft als opbouw year/month/day.samples, bijvoorbeeld bestand 2014/12/31.samples.

Voorbeeld van de inhoud::

  timestamp,year,month,day,hour,minute,second,EIT1,EIT2,EOT1,EOT2,EIC,EOC,GIT
  1420036801959,2014,12,31,15,40,1,1908.761,1300.25,0.0,0.0,0.19,0.0,481.597
  1420037102029,2014,12,31,15,45,2,1908.761,1300.269,0.0,0.0,0.28,0.0,481.597
  1420037401219,2014,12,31,15,50,1,1908.761,1300.293,0.0,0.0,0.27,0.0,
  1420037701492,2014,12,31,15,55,1,1908.761,1300.317,0.0,0.0,0.27,0.0,
  1420038001696,2014,12,31,16,0,1,1908.761,1300.339,0.0,0.0,0.32,0.0,
  
Hier staan codes voor het volgende:

- E of G voor Electricity of Gas
- I of O voor In (gebruik) of Out (levering); ik heb geen zonnepanelen, maar anders zou ook productie te zien zijn
- T of C voor Total (meterstand) of Current (huidig gebruik)
- niets, 1 of 2 voor respectievelijk totaal, nachttarief, dagtarief

Je ziet in het voorbeeld missende waarden voor gas, en dat m'n actuele gebruik in de samples van die 20 minuten varieerde van 190 Watt tot 320 Watt.

Programmaatje kan nu:

- Samples parsen en opslaan in CSV-bestanden.
- Samples van een dag omzetten naar een XML-bestand per dag.
- Samples van een dag omzetten naar drie grafieken (electriciteit en gas per uur en electriciteit samples)

Voorbeelden van de grafieken:
![Electriciteit](doc/02.elcurrent.png "Samples electriciteit")

<img src="https://github.com/fredvos/meet/blob/master/doc/02.elhours.png">

<img src="https://github.com/fredvos/meet/blob/master/doc/02.gashours.png">

